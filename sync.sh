#!/usr/bin/env bash
set -euo pipefail

pull_base_uri="git@github.com:privacybydesign"
push_base_uri="git@gitlab.science.ru.nl:irma/github-mirrors"

repositories=(
  atumd
  dockerfiles
  irma-demo-schememanager
  irma-demos
  irma-website
  irma_big_issuer
  irma_duo_issuer
  irma_email_issuer
  irma_keyshare_server
  irma_keyshare_webclient
  irma_mobile
  irma_sms_issuer
  irmago
  irmajs
  irmatube
  pbdf-website
)

for repository in "${repositories[@]}"; do
  echo "Handling fetch and push of ${repository} repository"

  if [ ! -d "./${repository}" ]; then
    git clone --no-checkout "${pull_base_uri}/${repository}.git" "${repository}"
    git -C "${repository}" remote add gitlab-mirror "${push_base_uri}/${repository}.git"
  else
    git -C "${repository}" fetch
  fi

  git -C "${repository}" push gitlab-mirror origin/master:master
  echo ""
done
