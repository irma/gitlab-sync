#!/usr/bin/env bash
set -euo pipefail

while true
do
  ./sync.sh
  sleep 3600
done
